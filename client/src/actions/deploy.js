import deployRequests from '../utils/deployRequests.js';

export const deployActions = {
    add,
    getAll,
    remove,
};

function add(deploy) {
    return dispatch => {
        dispatch(request(deploy));

        deployRequests.add(deploy)
            .done(response => {
                dispatch(success(response.deploy));
            })
            .fail(response => {
                dispatch(failure(response.responseJSON.errorMessage));
            });
    };

    function request(deploy) { return { type: "ADD_REQUEST", deploy } }
    function success(deploy) { return { type: "ADD_SUCCESS", deploy } }
    function failure(error) { return { type: "ADD_FAILURE", deploy, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        deployRequests.getAll()
            .done(response => dispatch(success(response.deploys)))
            .fail(response => {
                dispatch(failure(response.responseJSON.errorMessage))
            });
    };

    function request() { return { type: "GETALL_REQUEST" } }
    function success(deploys) { return { type: "GETALL_SUCCESS", deploys } }
    function failure(error) { return { type: "GETALL_FAILURE", error } }
}

function remove(id) {
    return dispatch => {
        dispatch(request(id));

        deployRequests.remove(id)
            .done(deploy => dispatch(success(id)))
            .fail(response => {
                dispatch(failure(id, response.responseJSON.errorMessage))
            });
    };

    function request(id) { return { type: "DELETE_REQUEST", id } }
    function success(id) { return { type: "DELETE_SUCCESS", id } }
    function failure(id, error) { return { type: "DELETE_FAILURE", id, error } }
}