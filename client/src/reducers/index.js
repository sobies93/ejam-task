import { combineReducers } from 'redux';

import { deploys } from './deploys';

const rootReducer = combineReducers({
  deploys,
});

export default rootReducer;