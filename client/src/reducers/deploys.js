const INITIAL_STATE = {};
const NEW_ID = 'NEW_ID';

//TODO consider flatening the state
export function deploys(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "GETALL_REQUEST":
      return {
        loading: true
      };
    case "GETALL_SUCCESS":
      return {
        items: action.deploys
      };
    case "GETALL_FAILURE":
      return {
        error: action.error
      };
    case "DELETE_REQUEST":
      return {
        ...state,
        items: state.items.map(deploy =>
          deploy._id === action.id
            ? { ...deploy, deleting: true }
            : deploy
        )
      };
    case "DELETE_SUCCESS":
      return {
        items: state.items.filter(deploy => deploy._id !== action.id)
      };
    case "DELETE_FAILURE":
      return {
        ...state,
        items: state.items.map(deploy => {
          if (deploy._id === action.id) {
            const { deleting, ...deployCopy } = deploy;
            return { ...deployCopy, error: action.error };
          }

          return deploy;
        })
      };
    case "ADD_REQUEST": {
      state.items.push(Object.assign({}, action.deploy, {_id: NEW_ID, new: true}));
      return {
        items: state.items,
      };
    }
    case "ADD_SUCCESS": {
      state.items.push(action.deploy);
      return {
        items: state.items.filter(deploy => deploy._id !== NEW_ID),
      };
    }
    case "ADD_FAILURE":
      return {
        ...state,
        items: state.items.map(deploy => {
          if (deploy._id === NEW_ID) {
            const { deleting, ...deployCopy } = deploy;
            return { ...deployCopy, error: action.error };
          }

          return deploy;
        })
      };
    default:
      return state
  }
}