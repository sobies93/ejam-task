import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
const moment = require('moment');

import { deployActions } from '../actions';

// TODO get templates from API
import templates from '../utils/templates.json';

function AddDeployForm() {
    const [deploy, setDeploy] = useState({
        url: '',
        templateName: '',
        version: ''
    });

    const [submitted, setSubmitted] = useState(false);
    const dispatch = useDispatch();

    function handleChange(e) {
        const { name, value } = e.target;
        setDeploy(deploy => ({ ...deploy, [name]: value }));
    }

    function handleSubmit(e) {
        e.preventDefault();

        setSubmitted(true);
        if (deploy.url && deploy.templateName && deploy.version) {
            deploy.deployedAt = moment().toString();
            dispatch(deployActions.add(deploy));
        }
    }

    return (
        <div>
            <h2>Prepare new deploy</h2>
            <form name="form" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>URL:</label>
                    <input type="text" name="url" value={deploy.url} onChange={handleChange} className={'form-control' + (submitted && !deploy.url ? ' is-invalid' : '')} />
                    {submitted && !deploy.url &&
                        <div className="invalid-feedback">Url is required</div>
                    }
                    {/*TODO add validation*/}
                </div>
                <div className="form-group">
                    <label>Template Name</label>
                    <select name="templateName" defaultValue='' className={'custom-select' + (submitted && !deploy.templateName ? ' is-invalid' : '')} onChange={handleChange}>
                      <option>-- Select --</option>
                        { templates.map((template, index) =>
                          <option key={index} value={template.name}>{template.name}</option>)
                        }
                    </select>
                    {submitted && !deploy.templateName &&
                        <div className="invalid-feedback">Template Name is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Version</label>
                    <select name="version" defaultValue='' className={'custom-select' + (submitted && !deploy.version ? ' is-invalid' : '')}
                        onChange={handleChange} disabled={!deploy.templateName}>
                      <option>{deploy.templateName ? '-- Select --' : 'Select Template Name first'}</option>
                        {deploy.templateName && templates.find(({name}) => name === deploy.templateName).versions.map((version, index) =>
                          <option key={index} value={version}>{version}</option>)
                        }
                    </select>
                    {submitted && !deploy.templateName &&
                        <div className="invalid-feedback">Template Name is required</div>
                    }
                </div>
                <div className="form-group">
                    <button className="btn btn-primary">
                        Deploy
                    </button>
                </div>
            </form>
        </div>
    );
}

export { AddDeployForm };