import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
const moment = require('moment');
import { deployActions } from '../actions';

function DeploysList() {
    const deploys = useSelector(state => state.deploys);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(deployActions.getAll());
    }, []);

    function handleDeleteDeploy(id) {
        dispatch(deployActions.remove(id));
    }

    return (
        <div>
            <h3>Deploys:</h3>
            {deploys.loading && <em>Loading deploys...</em>}
            {deploys.error && <span className="text-danger">ERROR: {deploys.error}</span>}
            {deploys.items &&
                <table  className="table table-dark table-sm">
                    {/*RWD: not great not terrible*/}
                    <thead>
                        <tr>
                          <th scope="col">URL</th>
                          <th scope="col">Template Name</th>
                          <th scope="col">Version</th>
                          <th scope="col">Deployed At</th>
                          <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    {deploys.items.map((deploy, index) =>
                        <tr key={deploy._id}>
                            <td>
                                {deploy.url}
                            </td>
                            <td>
                                {deploy.templateName}
                            </td>
                            <td>
                                {deploy.version}
                            </td>
                            <td>
                                {moment().format('YYYY/MM/DD hh:mm A')}
                            </td>
                            <td>
                            {
                                deploy.deleting ? <em>  Deleting...</em>
                                : deploy.error ? <span className="text-danger"> ERROR: {deploy.error}</span>
                                : deploy.new ? <span className="text-danger"> Deploying...</span>
                                : <span> <a onClick={() => handleDeleteDeploy(deploy._id)} className="text-primary">Delete</a></span>
                            }
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
            }
        </div>
    );
}

export { DeploysList };