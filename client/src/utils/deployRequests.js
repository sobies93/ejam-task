import $ from "jquery";

const url = '/api/deploy'

export default {
	getAll: () => $.ajax({
		url,
		method: 'GET',
	}),
	remove: (id) => $.ajax({
		url: url + `/${id}`,
		method: 'DELETE',
	}),
	add: (data) => $.ajax({
		url,
		method: 'POST',
		data
	}),
};