import React from 'react';

import { AddDeployForm } from './components/AddDeployForm.jsx';
import { DeploysList } from './components/DeploysList.jsx';

function App() {
    return (
        <div className="jumbotron">
            <div className="row">
                <div className="col-xs-12 col-lg-4">
                    <AddDeployForm />
                </div>
                <div className="col-xs-12 col-lg-8">
                    <DeploysList />
                </div>
            </div>
        </div>
    );
}

export { App };