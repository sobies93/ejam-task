const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

const public_dist = path.resolve(__dirname, '../dist/public');

module.exports = {
  entry: path.resolve(__dirname, './src/index.jsx'),
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: [
            "@babel/preset-env",
            "@babel/preset-react"
          ],
        },
      }
    ]
  },
  output: {
    path: public_dist,
  },
  plugins: [
  // these files should also be watched
    new CopyPlugin([
        { from: path.resolve(__dirname, 'html'), to: public_dist },
        { from: path.resolve(__dirname, 'css'), to: public_dist },
        // { from: path.resolve(__dirname, 'images'), to: public_dist },
      ],
    ),
  ]
};
