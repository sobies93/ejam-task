const express = require('express');
const router = express.Router();
const Deploy = require('../models/deploy.js');
const errorUserFriendly = 'An error has occurred';

router.get('/deploy', async (req, res) => {
	try {
		const deploys = await Deploy.find();
		res.send({status: 'success', deploys})
	} catch (error) {
		res.status(500).json({status: 'error', error, errorMessage: errorUserFriendly})
	}
});

router.post('/deploy', async (req, res, next) => {
	try {
		const deploy = new Deploy(req.body);
		await deploy.save();
		res.send({status: 'success', deploy})
	} catch (error) {
		res.status(500).json({status: 'error', error, errorMessage: 'Deploy failed'})
	}
});

router.delete('/deploy/:id', async (req, res, next) => {
	try {
		const { id } = req.params;
		await Deploy.findOneAndRemove({_id: id});
		res.send({status: 'success'})
	} catch (error) {
		res.status(500).json({status: 'error', error, errorMessage: 'Deleting failed'})
	}
});


module.exports = router;