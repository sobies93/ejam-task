const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DeploySchema = Schema({
	url: String,
	templateName: String,
	version: String,
	deployedAt: Date,
  },
);

module.exports = mongoose.model('deploy', DeploySchema);

