const express = require('express');
const router = express.Router();
const deploy = require('../api/deploy.js')


router.use('/template', express.static(__dirname + '/../utils/templates.json'));

router.use('/', deploy);

module.exports = router;
